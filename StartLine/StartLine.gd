extends Area2D


onready var checkpoints = get_node("../Checkpoints")

func _on_StartLine_body_entered(body):
	if body.has_method("on_lap"):
		if body.checkpoints_crossed.size() == checkpoints.get_child_count():
			body.on_lap()
