extends Area2D

onready var world = get_tree().get_root().get_node("World")
onready var time = world.get_node("RaceTime")
onready var car_times = {}


func _on_CheckPoint_body_entered(car):
	if not car.has_method("checkpoint"):
		return # only give a fuck about cars
	
	if car.checkpoints_crossed.has(self.name):
		return # skip if car has already crossed checkpoint this lap

	# note checkpoint time
	var now = time.get_split_time()
	if not car.name in car_times:
		car_times[car.name] = []
	car_times[car.name].append(now)
	
	# figure out lap count
	var lapidx = car_times[car.name].size() - 1
	
	# get the other cars times
	var lap_times = []
	for carname in car_times:
		if car_times[carname].size() <= lapidx:
			continue
		lap_times.append(car_times[carname][lapidx])
	lap_times.sort()
	
	# placement of this car
	var car_placement = lap_times.find(now)

	# find car ahead
	if car_placement > 0:
		var car_ahead_time = lap_times[car_placement-1]
		var car_ahead_name = null
		for carname in car_times:
			if car_times[carname].size() > lapidx and car_times[carname][lapidx] == car_ahead_time:
				car_ahead_name = carname
				break
		if car_ahead_name != null:
			# phew, found car ahead.
			# sorry for shitcode lol
			var car_ahead = world.get_node("Cars").get_node(car_ahead_name)
			
			# make diff string
			var diff_to_this = car_ahead_time - now
			car_ahead.show_text(time.format_ms(diff_to_this), Color("#00ff00"))
			car.show_text("+"+time.format_ms(-1*diff_to_this), Color("#000080"))
	if car_placement == 0:
		car.show_text(time.format_ms(now), Color("#000"))

	# update checkpoint on car
	car.checkpoint(self.name)
