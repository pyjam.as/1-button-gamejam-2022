extends Label

var countdown_start = 5

func start_countdown():
	$Timer.wait_time = countdown_start
	$Timer.start()

func _process(delta):
	if $Timer.is_stopped():
		self.text = ""
	else:
		self.text = str(int($Timer.time_left) + 1)

func _on_Timer_timeout():
	print("Starting all cars")
	var cars = get_node("../Cars").get_children()
	for car in cars:
		car.activate()

	# blank out countdown label
	self.text = ""
	
	# note time of race start
	get_node("../RaceTime").set_start_time()
