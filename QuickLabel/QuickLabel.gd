extends Node2D


func show_text_over_car(car, text, color=Color("#ff0000")):
	$Label.add_color_override("font_color", color)
	$Label.text = text
	var width = $Label.get_font("font").get_string_size(text).x
	$Label.margin_left = car.global_position.x - (width/2)
	$Label.margin_top = car.global_position.y
	self.get_node("AnimationPlayer").play("Flash")


func _on_AnimationPlayer_animation_finished(anim_name):
	self.queue_free()
