extends RigidBody2D

export var active = false

export var rotate_speed = 0.3
export var max_rotate_speed = 4
export var acceleration = 100
export var forward_friction = 10
export var sideways_friction = 160
onready var rotating = false

# scancode for character pressed
export var physical_scancode = 0

# Number of crashes, and max number before showing end animation
var crashes = 0
var lap_crashes = 0
export var max_number_of_crashes = 20
# Number of graphic damage states in sprite needs to be global
var sprite_damage_states

var lap_count = 0
var checkpoints_crossed = []

onready var world = get_tree().get_root().get_node("World")
onready var time = world.get_node("RaceTime")
var quicklabel_scene = preload("res://QuickLabel/QuickLabel.tscn")

func _ready():
	# Necessary to record collisions
	set_contact_monitor(true)
	set_max_contacts_reported(5)
	randomize()
	# Setup visual appearance of car, and damage states
	# Car has two animated sprites. One that can be coloured in, and one that shouldn't be.
	var colours = ["#10121c","#2c1e31","#6b2643","#ac2847","#ec273f","#94493a","#de5d3a","#e98537","#f3a833","#4d3533","#6e4c30","#a26d3f","#ce9248","#dab163","#e8d282","#f7f3b7","#1e4044","#006554","#26854c","#5ab552","#9de64e","#008b8b","#62a477","#a6cb96","#d3eed3","#3e3b65","#3859b3","#3388de","#36c5f4","#6dead6","#5e5b8c","#8c78a5","#b0a7b8","#deceed","#9a4d76","#c878af","#cc99ff","#fa6e79","#ffa2ac","#ffd1d5","#f6e8e0"]
	$AnimatedSprite_coloured.modulate = Color(colours[round(rand_range(0,colours.size()-1))])
	$AnimatedSprite_coloured.set_frame(0)
	$AnimatedSprite_non_coloured.set_frame(0)
	sprite_damage_states = $AnimatedSprite_coloured.get_sprite_frames().get_frame_count("static")
	

func activate():
	self.active = true

	self.add_central_force(acceleration_vector())
	self.apply_central_impulse(acceleration_vector())



func checkpoint(checkpoint_name):
	if not self.checkpoints_crossed.has(checkpoint_name):
		self.checkpoints_crossed.append(checkpoint_name)


func show_text(text, color=Color("#000")):
	var label = quicklabel_scene.instance()
	world.add_child(label)
	label.show_text_over_car(self, text, color)


func on_lap():
	# count stuff
	self.lap_count += 1

	# check if we've won
	if self.lap_count == time.num_laps:
		# YES! disable car and movement
		self.active = false
		time.finished_cars.append(self.name)
		self.show_text("Finished!\n%s. Place" % [time.finished_cars.size()], Color("#00ff00"))
		self.applied_force = Vector2(0,0)

	else: # show lap counter
		var text = "Lap %d" % [self.lap_count]
		if self.lap_crashes == 0:
			text += "\nCrash-free!"
		self.show_text(text)

	# reset stuff
	self.lap_crashes = 0
	self.checkpoints_crossed.clear()



func _input(event):
	if not self.active:
		return # don't do anything when disabled

	if not (event is InputEventKey):
		return # we only care about key events

	if event.physical_scancode != self.physical_scancode:
		return # event was not for us, don't do shit

	if event.pressed:
		self.rotating = true
	else:
		self.rotating = false


func _on_Car_body_entered(body):
	# In case of collision, deteriorate car. 
	self.lap_crashes += 1
	print("Chrashed", lap_crashes)
	if crashes < max_number_of_crashes:
		crashes += 1
		# Evil calculation. Cumulative crashes / max crashes / number of graphics states
		$AnimatedSprite_coloured.set_frame(int(round(crashes/(max_number_of_crashes/sprite_damage_states))))
		$AnimatedSprite_non_coloured.set_frame(int(round(crashes/(max_number_of_crashes/sprite_damage_states))))
	else:
		$AnimatedSprite_coloured.play("broken")
		$AnimatedSprite_non_coloured.play("broken")

func acceleration_vector():
	
	var motor_force = Vector2(self.acceleration, 0).rotated(self.rotation)

	# Friction force
	var along_wheels = linear_velocity.normalized().project(motor_force)
	var against_wheels = linear_velocity.normalized().project(motor_force.tangent())

	var friction_force = forward_friction*along_wheels + sideways_friction*against_wheels

	return motor_force - friction_force

func _physics_process(delta):
	if self.active:
		self.applied_force = acceleration_vector()
		if self.rotating:
			self.angular_velocity += rotate_speed
			if self.angular_velocity > self.max_rotate_speed:
				self.angular_velocity = max_rotate_speed
		else:
			self.angular_velocity *= 0.9
