extends Node

export var num_laps = 5
var start_time = 0
var finished_cars = []

func set_start_time():
	start_time = OS.get_system_time_msecs()

func get_split_time():
	return OS.get_system_time_msecs() - start_time

func get_formatted_split():
	return format_ms(get_split_time())

func format_ms(ms):
	var minutes = int(ms / (60 * 1000))
	var minutes_str = str(abs(minutes)).pad_zeros(2)
	var seconds = int(ms / 1000) % 60
	var seconds_str = str(abs(seconds)).pad_zeros(2)
	var milliseconds_str = str(int(round(abs(ms))) % 1000).pad_zeros(3)
	
	# determine sign
	var sign_char = ""
	if ms < 0:
		sign_char = "-"

	# format
	if minutes == 0:
		return "%s%s.%s" % [sign_char, seconds_str, milliseconds_str]
	else:
		return "%s%s:%s.%s" % [sign_char, minutes_str, seconds_str, milliseconds_str]
