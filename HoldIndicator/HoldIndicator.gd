extends Node2D

onready var active = true
var keydown_time = null
var hold_limit_ms = 2000

func nothing():
	pass

var tap_callback = funcref(self, "nothing")
var hold_callback = funcref(self, "nothing")


func _input(event):
	if not active:
		return

	if not (event is InputEventKey):
		return # we only care about key events
	
	if event.pressed and keydown_time == null:
		keydown_time = OS.get_system_time_msecs()
	elif not event.pressed:
		keydown_time = null
		if active:
			self.tap_callback.call_func()


func _process(delta):
	self.position = get_node("../../Camera2D").position + Vector2(-300, 200)

	if keydown_time == null:
		$Circle.scale = Vector2(0,0)
		$Ring.scale = Vector2(0,0)
		return

	$Ring.scale = Vector2(1,1)

	var now = OS.get_system_time_msecs()
	var holdtime = now - keydown_time

	var circlescale = float(holdtime) / float(hold_limit_ms)
	$Circle.scale = Vector2(circlescale, circlescale)

	if holdtime > hold_limit_ms:
		keydown_time = null
		print(self.hold_callback)
		self.hold_callback.call_func()
