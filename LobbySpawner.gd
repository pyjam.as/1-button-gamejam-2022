extends Node

export var active = false
var car_scene = preload("res://Car/Car.tscn")
onready var cars = get_node("../Cars")

var latest_event = null

func car_exists(physical_scancode):
	for car in cars.get_children():
		if car.physical_scancode == physical_scancode:
			return true
	return false

func find_map():
	for node in get_node("..").get_children():
		if "Map" in node.name:
			return node
	return null

func random_position_on_startline():
	# figure out spawn location
	var startline = find_map().get_node("StartLine")
	var startline_collision = startline.get_node("CollisionShape2D")
	var startline_segment = startline_collision.shape
	
	# get global coords of start line
	var global_a = (startline_segment.a + startline_collision.global_position)
	var global_b = (((startline_segment.b * startline_collision.global_scale).rotated(startline_collision.global_rotation) + startline_collision.global_position))

	# find a random position along that line
	var weight = rand_range(0, 1)
	var result_pos = global_a.linear_interpolate(global_b, weight)
	return [result_pos, startline_collision.global_rotation]


func start_game():
	# disable spawner, start countdown cars
	self.active = false
	get_node("../CountDown").start_countdown()
	# Hide instructions
	get_node("%Label_instructions").visible = false


func _ready():
	$HoldIndicator.tap_callback = funcref(self, "add_car")
	$HoldIndicator.hold_callback = funcref(self, "start_game")


func _process(delta):
	$HoldIndicator.active = self.active


func _input(event):
	if not active:
		return

	if not event is InputEventKey:
		return
	
	self.latest_event = event

func add_car():
	if (latest_event == null):
		return # fuck
	if car_exists(latest_event.physical_scancode):
		return # don't do anything if car already exists
	print("Creating car: " + char(latest_event.physical_scancode))
	var car = car_scene.instance()
	car.name = OS.get_scancode_string(latest_event.scancode)
	var posrot = random_position_on_startline()
	car.position = posrot[0]
	car.rotation = posrot[1]
	car.physical_scancode = latest_event.physical_scancode
	cars.add_child(car)


func _hide_instructions():
	get_node("%Label_instructions").visible = false
