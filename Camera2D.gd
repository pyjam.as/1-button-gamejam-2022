extends Camera2D

onready var cars = get_node("../Cars")

# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	var num_cars = cars.get_child_count()
	
	if num_cars < 2:
		pass
	else:
		var min_x = NAN
		var min_y = NAN
		
		var max_x = NAN
		var max_y = NAN
		
		for car in cars.get_children():
			min_x = min(min_x, car.position.x)
			min_y = min(min_y, car.position.y)
			
			max_x = max(max_x, car.position.x)
			max_y = max(max_y, car.position.y)
		
		var size = get_viewport().get_visible_rect().size

		var zoom_x = (max_x - min_x + 200)/size.x
		var zoom_y = (max_y - min_y + 200)/size.y
		
		var x = (max_x - min_x)/2 + min_x
		var y = (max_y - min_y)/2 + min_y
		
		var zoom = max(max(zoom_x, zoom_y), 0.8)

		set_position(Vector2(x,y))
		set_zoom(Vector2(zoom, zoom))
