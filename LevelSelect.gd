extends Node

var active = true

onready var world = get_tree().get_root().get_node("World")
onready var all_maps = ["res://Maps/Map1.tscn", "res://Maps/Map2.tscn"]
var map_idx = 0


func _ready():
	$HoldIndicator.tap_callback = funcref(self, "cycle_maps")
	$HoldIndicator.hold_callback = funcref(self, "select_map")


func cycle_maps():
	# select a new map
	map_idx = (map_idx + 1) % all_maps.size()
	var new_map = load(all_maps[map_idx]).instance()
	
	# Colour label according to map
	if all_maps[map_idx][14] == "1":
		get_node("%Label_select_racetrack").modulate = Color("#a3eb87")
		get_node("%Label_instructions").modulate = Color("#a3eb87")
	else:
		get_node("%Label_select_racetrack").modulate = Color("#7a532f")
		get_node("%Label_instructions").modulate = Color("#7a532f")
		
	new_map.name = "Map"
	for node in world.get_children():
		if "Map" in node.name:
			node.queue_free()
			break
	world.add_child(new_map)
	# ensure that it's in front of everything
	world.move_child(new_map, 0)


func select_map():
	active = false
	$HoldIndicator.active = false

	world.get_node("LobbySpawner").active = true
	# Show instructions
	get_node("%Label_instructions").visible = true
	
func _process(delta):
	if not self.active:
		$Label_select_racetrack.visible = false
